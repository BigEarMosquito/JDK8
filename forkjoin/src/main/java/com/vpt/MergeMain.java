package com.vpt;

import java.util.Random;

/**
 *merge练习
 */
public final class MergeMain {
	
    private static final Random random = new Random();
	public static void main(String[] args) {
		
		MergeSort sorter = new MergeSort(2);
		int[] array=generateArrays(20000000);
		sorter.sort(array);		
	
		for(int i=0,len=array.length;i<len;i++){
			System.out.print(i<len-1?array[i]+",":array[i]);		
		}
		

	}
	
	private static int[] generateArrays(int elements){
		int[] array=new int[elements];
		for(int i=0;i<elements;i++){
			array[i]=random.nextInt(Integer.MAX_VALUE);
		}
		
		return array;
	}

}
