package com.vpt;

import java.util.concurrent.locks.ReentrantReadWriteLock;

public class CacheData {
	Object data;
	volatile boolean cacheValid;
	ReentrantReadWriteLock rwl=new ReentrantReadWriteLock();
	
	void processCachedData(){
		rwl.readLock().lock();
		
		if(!cacheValid){
			rwl.readLock().unlock();
			rwl.writeLock().lock();
			if(!cacheValid){
				data=new Object();
				cacheValid=true;
			}
			
			rwl.readLock().lock();
			rwl.writeLock().unlock();
		}
		
		rwl.readLock().unlock();
	}
}
