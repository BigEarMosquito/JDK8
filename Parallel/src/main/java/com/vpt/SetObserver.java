package com.vpt;

public interface SetObserver<E> {
	void added(ObservableSet<E> set,E element);
}
