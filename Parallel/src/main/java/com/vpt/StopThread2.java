package com.vpt;

import java.util.concurrent.TimeUnit;

public class StopThread2 {
	// 1.不使用任何同步
	private volatile static boolean stopRequested = false;
	// 2.使用synchronized读写
	// private static synchronized void requestStop(){
	// stopRequested=true;
	// }
	//
	// private static synchronized boolean stopRequested(){
	// return stopRequested;
	// }
	// 3.使用volatile
	// private static volatile boolean stopRequested=false;

	public static void main(String[] args) throws InterruptedException {

		new Thread(new Runnable() {
			@SuppressWarnings("unused")
			int i = 0;

			public void run() {

				while (!stopRequested) {
					i++;
				}

			}
		}).start();

		TimeUnit.SECONDS.sleep(1);
		// stopRequested = true;
		stopRequested = true;

	}
}
