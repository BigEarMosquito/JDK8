package com.vpt;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SerialSample {
	private static volatile int nextSerialNumber=0;
	
	public static int generateSerialNumber(){
		try {
			TimeUnit.NANOSECONDS.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return nextSerialNumber++;
	}
	
	public static void main(String[] args) {
		ExecutorService pool = Executors.newFixedThreadPool(40);
		
		for(int i=0;i<20;i++){
			pool.submit(new Runnable(){

				public void run() {
					while(true){
						System.out.println(SerialSample.generateSerialNumber());
					}
					
			}});
		}
	}
}
