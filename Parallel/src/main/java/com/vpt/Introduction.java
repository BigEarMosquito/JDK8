package com.vpt;

import java.util.HashSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

public class Introduction {
	/*
	 * jsr:Java 规范提案 StringBuffer 和 StringBuilder
	 * 
	 * 一、多线程 1、操作系统有两个容易混淆的概念，进程和线程。
	 * 进程：一个计算机程序的运行实例，包含了需要执行的指令；有自己的独立地址空间，包含程序内容和数据；不同进程的地址空间是互相隔离的；
	 * 进程拥有各种资源和状态信息，包括打开的文件、子进程和信号处理。
	 * 线程：表示程序的执行流程，是CPU调度执行的基本单位；线程有自己的程序计数器、寄存器、堆栈和帧。同一进程中的线程共用相同的地址空间，
	 * 同时共享进进程锁拥有的内存和其他资源。
	 * <herf="http://blog.csdn.net/escaflone/article/details/10418651" />
	 * 
	 * 先行发生原则(happens-before) != 时间上的先后顺序 >程序次序规则 >管程锁定规则 >volatile变量规则 >线程启动规则
	 * >线程终止规则 >线程中断规则 >对象终结规则 >传递性
	 * 
	 * private int value=0;
	 * 
	 * public void setValue(int value){ this.value=value; }
	 * 
	 * public int getValue(){ return value; }
	 * 
	 * Amdahl定律
	 *
	 * 浏览器线程： javascript引擎线程 界面渲染线程 浏览器事件触发线程 http请求线程
	 * 
	 */

	// 示例1 {@link Counter}
	@Test
	public void testCounter() throws InterruptedException {
		
		for (int i = 0; i < 1000; i++) {
			new Thread(new Runnable() {

				public void run() {
					Counter.inc();
				}
			}).start();
		}
		TimeUnit.SECONDS.sleep(2);
		System.out.println("Counter.count=" + Counter.count);
	}

	/*
	 * 同步概念：synchronized不仅可以阻止一个线程看到对象处于不一致的状态之中，
	 * 它还可以保证进入同步方法或者同步代码块的每个线程，都看到由同一个锁保护之前所有的修改效果。
	 * 
	 * @note long,double非原子性协议 volatile:缓存一致性
	 * 
	 * 并发原则：
	 * 
	 * 1.同步访问共享的可变数据
	 * 
	 * {@link StopThread} {@link SerialSample} {@link FileChecker}
	 * 
	 * 当多个线程共享可变数据的时候，每个读或者写数据的线程都必须执行同步。
	 * 
	 * 
	 * 2.避免过度同步 为了避免活性失败和安全性失败，在一个同步的方法或者代码块中，永远不要放弃对客户端的控制。
	 * 通常，你应该在同步区域内做尽可能少的工作。
	 * 
	 * 3.executor和task优于线程 关于exector的讨论请自行学习
	 * 
	 * 4.并发工具优先于wait和notify 既然正确地使用wait和notify比较困难，就应该用更高级的并发工具来代替。 更高级的工具：
	 * I:Executor Framework II:并发集合(Concurrent Collection) III:同步器（Synchronizer)
	 * 并发集合中不可能排除并发活动；将它锁定没有什么作用。
	 * 
	 * {@link CountDownLatch,Semaphore} {@link CyclicBarrier,Exchanger}
	 * 
	 * 5.线程安全性的文档化
	 * 
	 * 在一个方法声明中出现synchronized修饰符,这是实现细节，并不是导出的API的一部分。
	 * 一个类为了可被多个线程安全的使用，必须在文档中清楚地说明它所支持的线程安全性级别。
	 * 
	 * 线程安全的级别：外部同步,内部同步 >不可变（immutable):String,Long,BigInteger
	 * >无条件的线程安全:实例可变，但是有足够的内部同步 Random,ConcurrentHashMap
	 * >有条件的线程安全:Collections.synchronized >非线程安全: ArrayList,HashMap
	 * >线程对立的:在Java平台的类库中,线程对立的类或者方法非常少.System.runFinalizersOnExit(已被废除)
	 * 
	 * 6.慎用延迟初始化 在大多数情况下,正常的初始化要优先于延迟初始化. schedule(分布式),有状态和无状态
	 * 如果利用延迟初始化来破坏初始化的循环,就要使用同步访问方法
	 * 
	 * private static class FileHolder{ static final FieldType
	 * filed=computeFiledValue(); }
	 * 
	 * static FieldType getField(){return FieldHolder.field;}
	 * 
	 * 如果处于性能的考虑而需要对实例域使用延迟初始化,就使用双重检查模式 private volatile FieldType field;
	 * FieldType getField(){ FieldType result=field; if(result==null){
	 * synchronized(this){ result=field; if(result==null){
	 * field=result=computeFieldValue(); } } } }
	 * 
	 * 7.不要依赖于线程调度器 任何依赖于线程调度器来达到正确性或者性能要求的程序,很有可能是不可移植的. 线程优先级是Java平台上最不可移植的特性.
	 * 
	 * 使用Thread.sleep(1)来代替Thread.yield()做并发测试
	 * 
	 * 8.避免使用线程组 线程组已经过时.有bug且不需修复.
	 * 
	 * 
	 */

	@Test
	public void testObservableSet() {
		ObservableSet<Integer> set = new ObservableSet<Integer>(new HashSet<Integer>());

		set.addObserver((o, e) -> System.out.println(e));

		for (int i = 0; i < 100; i++) {
			set.add(i);
		}
	}

	@Test
	public void testObservableSet2() {
		ObservableSet<Integer> set = new ObservableSet<Integer>(new HashSet<Integer>());
		set.addObserver(new SetObserver<Integer>() {

			@Override
			public void added(ObservableSet<Integer> set, Integer element) {
				System.out.println(element);
				if (element == 23)
					set.removeObserver(this);

			}
		});

		for (int i = 0; i < 100; i++) {
			set.add(i);
		}
	}

	@Test
	public void testObservableSet3() {
		ObservableSet<Integer> set = new ObservableSet<Integer>(new HashSet<Integer>());
		set.addObserver(new SetObserver<Integer>() {

			@SuppressWarnings("rawtypes")
			@Override
			public void added(ObservableSet set, Integer element) {
				System.out.println(element);
				if (element == 23) {
					ExecutorService executor = Executors.newSingleThreadExecutor();
					final SetObserver<Integer> observer = this;
					try {
						executor.submit(new Runnable() {

							@SuppressWarnings("unchecked")
							@Override
							public void run() {
								set.removeObserver(observer);
							}

						}).get();
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (ExecutionException e) {
						e.printStackTrace();
					} finally {
						executor.shutdown();
					}
				}

			}
		});
		for (int i = 0; i < 100; i++) {
			set.add(i);
		}
	}

	/*
	 * 始終应该使用wait循环模式来调用wait方法；永远不要在循环之外调用wait方法。 synchronized(obj){
	 * while(<conditions){ obj.wati(); }
	 * 
	 * }
	 * 
	 * 没有理由在新代码中使用wait和notify，即使有，也是极少的。
	 * 
	 * {@Executors} {@ExecutorService} {@Runnable} {@Callable} {@Thread}
	 * {@CountDownLatch} {@CyclicBarrier} {@Exchanger} {@AtomicInteger 等}
	 * {@BlockingQueue} {@ConcurrentHashMap} {@Lock}
	 * 
	 * 循环屏障在作用上类似倒数闸门，不过他不像倒数闸门是一次性的，可以循环使用。另外，线程之间是互相平等的，彼此都需要等待对方完成，
	 * 当一个线程完成自己的任务之后，等待其他线程完成。当所有线程都完成任务之后，所有线程才可以继续运行。
	 */

	/*
	 * 当一个变量定义为volatile之后，它将具备两种特性
	 * 1.保证此变量对所有线程的可见性，这里的“可见性”是指当一条线程修改了这个变量的值，新值对于其他线程来说是可以立即得知的。而普通变量不能做到这一点
	 * ，普通变量的值在线程间传递钧需要通过主内存来完成。
	 * 2.禁止指令重排序优化，普通变量仅仅会保证在该方法的执行过程中所有依赖值结果的地方都能获取到正确的结果，
	 * 而不能保证变量赋值操作的顺序与程序代码的执行顺序一致。因为在一个线程的方法执行过程中无法感知到这点，这也就是java内存模型中描述的所谓的“
	 * 线程内表现为串行的语义”。 see {StopThread}示例
	 * volatile：volatile变量在各个线程的工作内存中不存在不一致问题（在各个线程的工作内存中，volatile变量也可以存在不一致的情况，
	 * 但由于每次使用之前都要先刷新，执行引擎看不到不一致的情况，因此可以认为不存在一致性问题），但是java里面的运算并原子操作，
	 * 导致volatile变量的运算在并发下一样是不安全的。
	 * 注：volatile语义在jdk1.5以前有bug，所以不建议在jdk1.5以前的环境中使用volatile。
	 */

}
