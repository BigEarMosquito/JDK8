package com.vpt;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.IdentityHashMap;
import java.util.Scanner;

public final class FileChecker {
	@SuppressWarnings({ "unused" })
	public static void main(String[] args) throws FileNotFoundException {
		if(args.length<1){
			System.exit(0);
		}
		
		final Scanner scanner = new Scanner(new File(args[0]));
		
		IdentityHashMap<Integer,Integer> identityMap=new IdentityHashMap<>(50_000_000);
		int lineNumber=0;
		while(scanner.hasNextLine()){
			
			lineNumber++;
			String nextLine = scanner.nextLine();
			int value = Integer.parseInt(nextLine);
			if(identityMap.containsKey(value)){
				identityMap.put(value, identityMap.get(value)+1);
			}else{
				identityMap.put(value, 1);
			}
		}
		identityMap.forEach((k,v)->{
			if(v>1){
				System.out.println(v+":"+k);
			}
		});
		System.out.println("分析完毕");
		scanner.close();
		
	}
}
