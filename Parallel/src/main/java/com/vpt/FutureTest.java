package com.vpt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class FutureTest {
	
	//file:H:\files\pdfs Java
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		System.out.print("Enter base directory:");
		String directory = in.nextLine();
		System.out.print("Enter keyword:");
		String keyword=in.nextLine();
		in.close();
		MatchCounter counter=new MatchCounter(new File(directory),keyword);
		FutureTask<Integer> task = new FutureTask<Integer>(counter);
		Thread t=new Thread(task);
		t.start();
		
		try {
			System.out.println(task.get()+" matching files.");
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}
}

class MatchCounter implements Callable<Integer> {
	private File directory;
	private String keyword;
	private int count;

	public MatchCounter(File directory, String keyword) {
		this.directory = directory;
		this.keyword = keyword;
	}

	@Override
	public Integer call() {
		count = 0;

		File[] files = directory.listFiles();
		List<Future<Integer>> results = new ArrayList<Future<Integer>>();

		for (File file : files) {
			if (file.isDirectory()) {
				MatchCounter counter = new MatchCounter(file, keyword);
				FutureTask<Integer> task = new FutureTask<Integer>(counter);
				results.add(task);
				Thread t = new Thread(task);
				t.start();
			} else {
				if (search(file))
					count++;
			}

			for (Future<Integer> result : results) {
				try {
					count += result.get();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return count;

	}

	public boolean search(File file) {
		System.out.println(file.getName());
		try (Scanner scanner = new Scanner(new FileInputStream(file))) {
			boolean found = false;

			while (!found && scanner.hasNextLine()) {
				String line = scanner.nextLine();
				// 沒有处理跨行的处理
				if (line.contains(keyword))
					found = true;
			}
			return found;
		} catch (FileNotFoundException e) {
			return false;
		}
	}

}
