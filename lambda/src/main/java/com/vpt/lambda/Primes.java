package com.vpt.lambda;

public final class Primes {
	
	private Primes(){
		
	}
	
	public static boolean isPrime(int number){
		int range=(int)Math.sqrt(number)+1;
		for(int i=2;i<range;i++){
			if(number%i==0){
				return false;
			}
		}
		return true;
	}
}
