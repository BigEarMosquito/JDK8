package com.vpt.bulk;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

public class WC {
	private static final int READ_AHEAD_LIMIT=100_000_000;
	
	private static final Pattern nonWordPattern=Pattern.compile("\\W");
	
	private static void collectInFourPasses(BufferedReader reader) throws IOException{
		//System.out.println("Character count="+reader.lines().flatMapToInt(String::chars).count());
		//reader.reset();
		//System.out.println("Word count="+reader.lines().flatMap(nonWordPattern::splitAsStream).filter(str->!str.isEmpty()).count());
		//reader.reset();
		reader.lines().forEach(System.out::println);
		reader.reset();
		
		System.out.println("Newline count="+reader.lines().count());
		
		reader.reset();
		
		System.out.println("Max line length="+reader.lines().mapToInt(String::length).max().getAsInt());
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		try(BufferedReader reader=new BufferedReader(new FileReader(args[0]))){
			reader.mark(READ_AHEAD_LIMIT);
			collectInFourPasses(reader);
		}
		
	}
}
