package com.vpt;

import java.util.concurrent.TimeUnit;

public final class Supportor {
	private Supportor(){
		throw new UnsupportedOperationException();
	}
	
	public static void printExeTime(long nanoSeconds){
		System.out.println("用時"+TimeUnit.NANOSECONDS.toMillis(nanoSeconds)+"毫秒");
	}
}
