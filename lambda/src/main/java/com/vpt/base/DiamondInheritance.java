package com.vpt.base;

/*
 *<pre>
 *                   Animal
 *                    /   \
 *                 Horse   Bird
 *                    \   /
 *                   Pegasus
 * </pre>
 */
public class DiamondInheritance {
	public interface Animal{
		String go();
	}
	
	public interface Horse extends Animal{
		@Override
		default String go(){
			return this.getClass().getSimpleName()+" walks on four legs";
		}
	}
	
	public interface Bird extends Animal{
		@Override
		default String go(){
			return this.getClass().getSimpleName()+" walks on tow legs";
		}
		
		default String fly(){
			return "I can fly";
		}
	}
	
	public static class Pegasus implements Horse,Bird{

		@Override
		public String go() {
			return Horse.super.go();
		}

		
	}
}
