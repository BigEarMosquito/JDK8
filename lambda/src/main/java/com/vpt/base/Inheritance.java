package com.vpt.base;

/*
 * <ul>
 * <li>Class wins. If the superclass has a concrete or abstract declaration of
 * this method, then it is preferred over all defaults.</li>
 * <li>Subtype wins. If an interface extends another interface, and both provide
 * a default, then the more specific interface wins. </li>
 * </ul>
 */
public class Inheritance {
	public interface Swimable{
		default String swim(){
			return "I can swim";
		}
	}
	
	public abstract static class Fish implements Swimable{

		@Override
		public String swim() {
			return this.getClass().getSimpleName()+" swims under water";
		}
		
	}
	
	public static class Tuna extends Fish implements Swimable{
		
	}
	
	public interface Diveable extends Swimable{

		@Override
		default String swim() {
			return "I can swim on the surface of the water";
		}
		
		default String dive(){
			return "I can dive";
		}
		
	}
	
	public static class Duck implements Swimable,Diveable{
		
	}
	
	public static void main(String[] args) {
		System.out.println(new Tuna().swim());
		
		System.out.println(new Duck().swim());
	}
}
