package com.vpt.base;
/*
 *演示interface有默认实现
 *
 */
public class SimpleUsage {
	public interface Animal{
		default String eat(){
			return this.getClass().getSimpleName()+" eats like on ordinary animal";
		}
	}
	
	public static class Dog implements Animal{
		
	}
	
	public static class Mosquito implements Animal{
		@Override
		public String eat(){
			return "Mosquito consumes blood";
		}
	}
	
	public static void main(String[] args) {
		System.out.println(new Dog().eat());
		
		System.out.println(new Mosquito().eat());
	}
}
