package com.vpt.base;

import java.lang.reflect.Field;

public class MixIn {
	public interface Debuggable {
		default String toDebugString() {
			StringBuilder sb = new StringBuilder();
			sb.append("State of the:").append(this.getClass().getSimpleName()).append("\n");

			for (Class<?> cls = this.getClass(); cls != null; cls=cls.getSuperclass()) {
				for (Field f : cls.getDeclaredFields()) {
					f.setAccessible(true);
					try {
						sb.append(f.getName()).append(":").append(f.get(this)).append("\n");
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			}
			return sb.toString();
		}
	}

	public static enum BuildType implements Debuggable {
		BUILD(0,"-build"),
		PLAN(0,"-plan"),
		EXCLUDE(1,"-exclude"),
		TOTAL(2,"-total");
		private final int compareOrder;
		private final String pathSuffix;

		private BuildType(int compareOrder, String pathSuffix) {
			this.compareOrder = compareOrder;
			this.pathSuffix = pathSuffix;
		}

		public int getCompareOrder() {
			return compareOrder;
		}

		public String getPathSuffix() {
			return pathSuffix;
		}
	}

	public static void main(String[] args) {
		System.out.println(BuildType.BUILD.toDebugString());
	}
}
