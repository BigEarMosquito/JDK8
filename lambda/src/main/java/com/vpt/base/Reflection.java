package com.vpt.base;

import java.lang.reflect.InvocationTargetException;
import java.util.stream.Stream;
/*
 * 使用Stream.of(),forEach()和Consumer
 */
public class Reflection {

	public interface Animal {
		default String eat() {
			return this.getClass().getSimpleName() + " eats like an ordinary animal";
		}

		default String sleep() {
			return this.getClass().getSimpleName() + " sleeps like an ordinary animal";
		}

		String go();
	}

	public static class Dog implements Animal {

		@Override
		public String go() {
			return "Dog walks on four legs";
		}

		@Override
		public String sleep() {
			return "Dog sleeps";
		}

	}

	public static void main(String[] args) throws NoSuchMethodException {
		Dog dog = new Dog();

		Stream.of(Dog.class.getMethod("eat"), Dog.class.getMethod("go"), Dog.class.getMethod("sleep")).forEach((m) -> {
			System.out.println("Method name:" + m.getName());
			System.out.println(" is Defualt:" + m.isDefault());
			try {
				System.out.println(" invoke: " + m.invoke(dog));

			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
			System.out.println();
		});

	}
}
