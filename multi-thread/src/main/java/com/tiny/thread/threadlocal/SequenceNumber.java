package com.tiny.thread.threadlocal;

import java.util.ArrayList;
import java.util.List;

/**
 * http://qifuguang.me/2015/09/02/[Java%E5%B9%B6%E5%8F%91%E5%8C%85%E5%AD%A6%E4%B9%A0%E4%B8%83]%E8%A7%A3%E5%AF%86ThreadLocal/
 * Created by zhumenglong on 2017/3/11.
 * 执行结果</p>
 * thread[Thread-2] sn[1]
 * thread[Thread-0] sn[1]
 * thread[Thread-1] sn[1]
 * thread[Thread-2] sn[2]
 * thread[Thread-0] sn[2]
 * thread[Thread-1] sn[2]
 * thread[Thread-2] sn[3]
 * thread[Thread-0] sn[3]
 * thread[Thread-1] sn[3]
 * 考查输出的结果信息，我们发现每个线程所产生的序号虽然都共享同一个Sequence Number实例，</P>
 * 但它们并没有发生相互干扰的情况，而是各自产生独立的序列号，这是因为我们通过ThreadLocal为每一个线程提供了单独的副本。
 */
public class SequenceNumber {
    //①通过匿名内部类覆盖ThreadLocal的initialValue()方法，指定初始值
    private static ThreadLocal<Integer> seqNum = new ThreadLocal<Integer>() {
        public Integer initialValue() {
            return 0;
        }
    };

    //②获取下一个序列值
    public int getNextNum() {
        seqNum.set(seqNum.get() + 1);
        return seqNum.get();
    }

    public static void main(String[] args) {
        SequenceNumber sn = new SequenceNumber();
        //③ 3个线程共享sn，各自产生序列号
        TestClient t1 = new TestClient(sn);
        TestClient t2 = new TestClient(sn);
        TestClient t3 = new TestClient(sn);
        List<TestClient> list = new ArrayList<>();
        t1.start();
        t2.start();
        t3.start();
    }

    private static class TestClient extends Thread {
        private SequenceNumber sn;

        public TestClient(SequenceNumber sn) {
            this.sn = sn;
        }

        @Override
        public void run() {
            //④每个线程打出3个序列值
            for (int i = 0; i < 3; i++) {
                System.out.println("thread[" + Thread.currentThread().getName() +
                        "] sn[" + sn.getNextNum() + "]");
            }
        }
    }
}
