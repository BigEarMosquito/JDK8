package com.tiny.thread.threadlocal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhumenglong on 2017/3/11.
 */
public class SequenceNumber2 {

    //①通过匿名内部类覆盖ThreadLocal的initialValue()方法，指定初始值
    private static SimpleThreadLocal seqNum = new SimpleThreadLocal() {
        public Integer initialValue() {
            return 0;
        }
    };

    //②获取下一个序列值
    public int getNextNum() {
        seqNum.set((Integer) seqNum.get() + 1);
        return (Integer) seqNum.get();
    }

    public static void main(String[] args) {
        SequenceNumber2 sn = new SequenceNumber2();
        //③ 3个线程共享sn，各自产生序列号
        TestClient t1 = new TestClient(sn);
        TestClient t2 = new TestClient(sn);
        TestClient t3 = new TestClient(sn);
        List<TestClient> list = new ArrayList<>();
        t1.start();
        t2.start();
        t3.start();
    }

    private static class TestClient extends Thread {
        private SequenceNumber2 sn;

        public TestClient(SequenceNumber2 sn) {
            this.sn = sn;
        }

        @Override
        public void run() {
            //④每个线程打出3个序列值
            for (int i = 0; i < 3; i++) {
                System.out.println("thread[" + Thread.currentThread().getName() +
                        "] sn[" + sn.getNextNum() + "]");
            }
        }
    }

}
