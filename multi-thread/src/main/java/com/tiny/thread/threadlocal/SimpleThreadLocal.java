package com.tiny.thread.threadlocal;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * http://blog.csdn.net/lufeng20/article/details/24314381
 * 实现一个简化版本的ThreadLocal，里面有一个Map，
 * 用于存储每一个线程的变量副本，Map中元素的键为线程对象，而值对应线程的变量副本。
 * Created by zhumenglong on 2017/3/11.
 */
public class SimpleThreadLocal {
    private Map valueMap = Collections.synchronizedMap(new HashMap());

    public void set(Object newValue) {
        //①键为线程对象，值为本线程的变量副本
        valueMap.put(Thread.currentThread(), newValue);
    }

    public Object get() {
        Thread currentThread = Thread.currentThread();

        //②返回本线程对应的变量
        Object o = valueMap.get(currentThread);

        //③如果在Map中不存在，放到Map中保存起来
        if (o == null && !valueMap.containsKey(currentThread)) {
            o = initialValue();
            valueMap.put(currentThread, o);
        }
        return o;
    }

    public void remove() {
        valueMap.remove(Thread.currentThread());
    }

    public Object initialValue() {
        return null;
    }
}
