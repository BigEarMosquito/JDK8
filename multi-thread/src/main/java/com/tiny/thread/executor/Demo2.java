package com.tiny.thread.executor;

/**
 * Demo2：ExecutorService与生命周期</p>
 * ExecutorService扩展了Executor并添加了一些生命周期管理的方法。一个Executor的生命周期有三种状态，运行 ，关闭 ，终止 。</p>
 * Executor创建时处于运行状态。当调用ExecutorService.shutdown()后，处于关闭状态，isShutdown()方法返回true。</p>
 * 这时，不应该再想Executor中添加任务，所有已添加的任务执行完毕后，Executor处于终止状态，isTerminated()返回true。</p>
 * Created by zhumenglong on 2017/3/11.
 */
public class Demo2 {
}
