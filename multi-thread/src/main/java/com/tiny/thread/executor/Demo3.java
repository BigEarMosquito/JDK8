package com.tiny.thread.executor;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 使用Callable，Future返回结果
 * Created by zhumenglong on 2017/3/11.
 */
public class Demo3 {

    /**
     * ExecutoreService提供了submit()方法，传递一个Callable，或Runnable，返回Future。
     * 如果Executor后台线程池还没有完成Callable的计算，这调用返回Future对象的get()方法，会阻塞直到计算完成。
     */
    public static void method1() {
        Callable<Integer> func =()->{
            System.out.println("inside callable");
            Thread.sleep(1000);
            return new Integer(8);
        };
        FutureTask<Integer> futureTask = new FutureTask<>(func);
        Thread newThread = new Thread(futureTask);
        newThread.start();

        try {
            System.out.println("blocking here");
            Integer result = futureTask.get();
            System.out.println(result);
        } catch (InterruptedException | ExecutionException ignored) {
        }
    }

    public static void main(String[] args) {
        method1();
    }
}
