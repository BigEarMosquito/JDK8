package com.tiny.thread.synchronize.synch;

import java.util.Arrays;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 使用同步锁的银行
 * Created by zhumenglong on 2017/3/12.
 */
public class Bank {
    private final double[] accounts;
    private Lock bankLock;
    private Condition sufficientFunds;

    /**
     * 初始化银行。
     *
     * @param n              账户数量
     * @param initialBalance 初始化余额
     */
    public Bank(int n, double initialBalance) {
        accounts = new double[n];
        Arrays.fill(accounts, initialBalance);
        bankLock = new ReentrantLock();
        sufficientFunds = bankLock.newCondition();
    }

    /**
     * 转账。
     *
     * @param from   来源账户
     * @param to     收款账户
     * @param amount 转账金额
     */
    public void transfer(int from, int to, double amount) throws InterruptedException {
        bankLock.lock();
        try {
            /*
             * 如果transfer方法发现余额不足，它调用sufficientFunds.await();方法
             * 当前线程现在阻塞，并放弃了锁，我们希望这样可以使得另一个线程可以进行增加账户余额的操作
             */
            while (accounts[from] < amount)
                sufficientFunds.await();
            System.out.print(Thread.currentThread());
            accounts[from] -= amount;
            System.out.printf(" %10.2f from %d to %d", amount, from, to);
            accounts[to] += amount;
            System.out.printf(" Total Balance: %10.2f%n", getTotalBalance());
            /*
             * 等待获得锁的线程和调用await方法的线程存在本质上的不同，一旦一个线程调用await方法，
             * 它进入了该条件的等待集。当锁可用时，该线程不能马上解除阻塞。相反，它处于阻塞状态，
             * 直到另一个线程调用同一条件上的signalAll方法为止。
             */
            sufficientFunds.signalAll();
        } finally {
            bankLock.unlock();
        }
    }

    /**
     * 获取总余额
     *
     * @return the total balance
     */
    public double getTotalBalance() {
        bankLock.lock();
        try {
            double sum = 0;

            for (double a : accounts)
                sum += a;

            return sum;
        } finally {
            bankLock.unlock();
        }
    }

    /**
     * Gets the number of accounts in the bank.
     *
     * @return the number of accounts
     */
    public int size() {
        return accounts.length;
    }
}
