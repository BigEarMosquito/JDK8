import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class LocalDate2Date {

    public static void main(String[] args) {
        /**
         * 计算当天的起始时间
         */
        LocalDate now = LocalDate.now();
        Date createBeginTime = Date.from(now.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Date createEndTime = Date.from(now.plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
}
