package com.vpt;

import java.io.InputStream;
import java.io.InputStreamReader;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Main {
	public static void main(String[] args) {

		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("js");
		
		engine.put("engine", engine);
		
		eval(engine,"conc.js");
		eval(engine,"gui.js");
		eval(engine,"scriptpad.js");
		eval(engine,"mm.js");
	}
	
	private static void eval(ScriptEngine engine,String name){
		InputStream is=Main.class.getResourceAsStream("/"+name);
		engine.put(ScriptEngine.FILENAME, name);
		
		try {
			engine.eval(new InputStreamReader(is));
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}
}
